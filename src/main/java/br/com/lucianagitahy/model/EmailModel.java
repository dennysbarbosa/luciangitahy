package br.com.lucianagitahy.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class EmailModel {

	@JsonProperty("emailJson")
	public EmailInf emailInf;
}
