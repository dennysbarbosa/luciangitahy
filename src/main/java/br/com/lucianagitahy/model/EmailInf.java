package br.com.lucianagitahy.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder("emailJson")
public class EmailInf {

	@JsonProperty("name")
	public String name;
	
	@JsonProperty("email")
	public String email;

	@JsonProperty("subject")
	public String subject;
	
	@JsonProperty("message")
	public String message;
	
}
