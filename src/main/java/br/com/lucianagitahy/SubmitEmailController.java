package br.com.lucianagitahy;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.lucianagitahy.contato.Mail;
import br.com.lucianagitahy.model.EmailModel;

@Controller 
public class SubmitEmailController {
	
	@RequestMapping(value = "/request/save", produces = {"application/json"}, method = RequestMethod.POST)
	public ResponseEntity<String> sendEmail(@RequestBody String emailJson
			) {
		
		ObjectMapper mapper = new ObjectMapper();
		EmailModel emailModel;
		boolean success = false;
		try {
			emailModel = mapper.readValue(emailJson, EmailModel.class);
			success = new Mail().sendEmail(emailModel);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return (ResponseEntity<String>) ResponseEntity.badRequest();
		}
		if(success) {  
		   return ResponseEntity.ok("");
		}else {
		   return (ResponseEntity<String>) ResponseEntity.badRequest();
		}
	}

}
