package br.com.lucianagitahy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class LucianagitahyApplication extends SpringBootServletInitializer{

	public static void main(String[] args) {
		SpringApplication.run(LucianagitahyApplication.class, args);
	}

}
