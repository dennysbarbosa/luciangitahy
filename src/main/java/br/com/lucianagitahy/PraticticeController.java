package br.com.lucianagitahy;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class PraticticeController {
	
	@RequestMapping("/practice-areas.html")
	public String index() {

		return "practice-areas";
	}

}
