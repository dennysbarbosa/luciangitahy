package br.com.lucianagitahy;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class AttorneysController {
	
	@RequestMapping("/attorneys.html")
	public String index() {

		return "attorneys";
	}

}
